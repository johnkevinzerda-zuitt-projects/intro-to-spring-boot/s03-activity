package com.zerda.s01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class S01Application {

	public static void main(String[] args) {
		SpringApplication.run(S01Application.class, args);
	}

	@GetMapping("/")
	public String root() {
		return String.format("This is the main route.");
	}

	@GetMapping("/hello")
	public String hello(@RequestParam(value="name", defaultValue = "world") String name) {
		return String.format("Hello %s!", name);
	}

	/*
	* Mini-activity:
	* Define a hi() method that will output "hi user" when a GET request is received at localhost:8080/hi.
	* A url parameter named user may be used which will behave similarly to our code along.
	* (i.e. ?user=Jane will output "hi Jane")
	* */

	@GetMapping("/hi")
		public String hi(@RequestParam(value="user", defaultValue = "user") String user) {
		return String.format("Hi, %s!", user);
	}
}
