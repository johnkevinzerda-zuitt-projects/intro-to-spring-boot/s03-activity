package com.zerda.s01.controllers;

import com.zerda.s01.models.Comment;
import com.zerda.s01.services.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class CommentController {
    @Autowired
    CommentService commentService;

    @RequestMapping(value = "/posts/{id}/comments", method = RequestMethod.POST)
    public ResponseEntity<Object> createComment(@PathVariable Long id, @RequestBody Comment newComment,
                                                @RequestHeader(value = "Authorization") String token) {
        commentService.createComment(id, newComment, token);
        return new ResponseEntity<>("New post was created.", HttpStatus.CREATED);
    }

    @RequestMapping(value = "/comments", method = RequestMethod.GET)
    public ResponseEntity<Object> getComments() {
        return new ResponseEntity<>(commentService.getComments(), HttpStatus.OK);
    }

    @RequestMapping(value = "/posts/my-comments", method = RequestMethod.GET)
    public ResponseEntity <Object> getUsersComments(@RequestHeader(value = "Authorization") String token) {
        return new ResponseEntity<>(commentService.getUserComments(token), HttpStatus.OK);
    }

    @RequestMapping(value = "/comments/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updateComment(@PathVariable Long id, @RequestBody Comment updatedComment,
                                                @RequestHeader(value = "Authorization") String token) {
        return new ResponseEntity<>(commentService.updateComment(id, updatedComment, token), HttpStatus.OK);
    }

    @RequestMapping(value = "/comments/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteComment(@PathVariable Long id,
                                                @RequestHeader(value = "Authorization") String token) {
        return new ResponseEntity<>(commentService.deleteComment(id, token), HttpStatus.OK);
    }
}
