package com.zerda.s01.controllers;

import com.zerda.s01.exceptions.UserException;
import com.zerda.s01.models.User;
import com.zerda.s01.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@CrossOrigin
public class UserController {
    @Autowired
    UserService userService;

    @RequestMapping(value = "/users/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updateUser(@PathVariable Long id, @RequestBody User updatedUser, @RequestHeader(value = "Authorization") String token) {
        return userService.updateUser(id, updatedUser, token);
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteUser(@PathVariable Long id, @RequestHeader(value = "Authorization") String token) {
        return userService.deleteUser(id, token);
    };

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public ResponseEntity<Object> getUsers(@RequestHeader(value = "Authorization") String token) {
        return new ResponseEntity<>(userService.getUsers(token), HttpStatus.OK);
    };

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<Object> register(@RequestBody Map<String, String> userDetails) throws UserException {
        // Retrieve the username entered
        String username = userDetails.get("username");

        if (!userService.findByUsername(username).isEmpty()) {
            throw new UserException("Username already exists.");
        } else {
            String password = userDetails.get("password");
            String hashedPassword = new BCryptPasswordEncoder().encode(password);
            User newUser = new User(username, hashedPassword);
            userService.createUser(newUser);
            return new ResponseEntity<>("New user was registered.", HttpStatus.CREATED);
        }
    }
}

