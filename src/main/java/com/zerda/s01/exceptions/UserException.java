package com.zerda.s01.exceptions;

public class UserException extends Exception{

    public UserException(String message) {
        super(message);
    }
}
