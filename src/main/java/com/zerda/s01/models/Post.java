package com.zerda.s01.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="posts")
public class Post {
    // Properties
    // id
    // title
    // content
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String title, content;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private  User user; //author

    @OneToMany(mappedBy = "post")
    @JsonIgnore
    private Set<Comment> comments;

    // Constructor
    public Post() {}
    public Post(String title, String content) {
        this.title = title;
        this.content = content;
    }

    // Getters & Setters
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User author) {
        this.user = author;
    }
}
