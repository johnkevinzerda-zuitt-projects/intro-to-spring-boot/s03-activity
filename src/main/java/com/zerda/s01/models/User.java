package com.zerda.s01.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="users")
public class User {

    // Properties
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String username;

    @Column
    private String password;

    @OneToMany(mappedBy = "user")
    @JsonIgnore
    private Set<Post> posts;

    @OneToMany(mappedBy = "user")
    @JsonIgnore
    private Set<Comment> comments;
    //Constructor

    public User() {}
    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
    // Getters & Setters

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Post> getPosts() {
        return posts;
    }

    public void setPosts(Set<Post> posts) {
        this.posts = posts;
    }

    public Set<Comment> getComments() {
        return comments;
    };


    public Long getId() {
        return id;
    }
}
