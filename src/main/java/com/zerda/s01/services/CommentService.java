package com.zerda.s01.services;

import com.zerda.s01.models.Comment;
import org.springframework.http.ResponseEntity;

import java.util.Set;

public interface CommentService {
    void createComment(Long id, Comment newComment, String token);
    ResponseEntity updateComment(Long id, Comment updatedComment, String token);
    ResponseEntity deleteComment(Long id, String token);
    Iterable<Comment> getComments();
    Set<Comment> getUserComments(String token);
}
