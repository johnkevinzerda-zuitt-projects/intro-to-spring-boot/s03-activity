package com.zerda.s01.services;

import com.zerda.s01.config.JwtToken;
import com.zerda.s01.models.Comment;
import com.zerda.s01.models.Post;
import com.zerda.s01.models.User;
import com.zerda.s01.repositories.CommentRepository;
import com.zerda.s01.repositories.PostRepository;
import com.zerda.s01.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class CommentServiceImplementation implements CommentService{
    @Autowired
    private CommentRepository commentRepo;
    @Autowired
    private PostRepository postRepo;
    @Autowired
    private UserRepository userRepo;
    @Autowired
    JwtToken jwtToken;

    public void createComment(Long id, Comment newComment, String token) {
        User author = userRepo.findByUsername(jwtToken.getUsernameFromToken(token));
        Post post = postRepo.findById(id).get();
        Comment newCommentObj = new Comment();
        newCommentObj.setComment(newComment.getComment());
        newCommentObj.setUser(author);
        newCommentObj.setPost(post);

        commentRepo.save(newCommentObj);
    }

    public ResponseEntity updateComment(Long id,Comment updatedComment, String token) {
        Comment existingComment = commentRepo.findById(id).get();
        String authorUsername = existingComment.getUser().getUsername();
        String authUsername = jwtToken.getUsernameFromToken(token);

        if (authUsername.equals(authorUsername)) {
            existingComment.setComment(updatedComment.getComment());

            commentRepo.save(existingComment);
            return new ResponseEntity<>("An existing comment was updated.", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You are not authorized to update this comment.", HttpStatus.UNAUTHORIZED);
        }
    }

    @Override
    public ResponseEntity deleteComment(Long id, String token) {
        Comment existingComment = commentRepo.findById(id).get();
        String authorUsername = existingComment.getUser().getUsername();
        String authUsername = jwtToken.getUsernameFromToken(token);

        if (authUsername.equals(authorUsername)) {
            commentRepo.deleteById(id);
            return new ResponseEntity<>("An existing comment was deleted.", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You are not authorized to delete this comment.", HttpStatus.UNAUTHORIZED);
        }
    }

    @Override
    public Iterable<Comment> getComments() {
        return commentRepo.findAll();
    }

    @Override
    public Set<Comment> getUserComments(String token) {
        User author = userRepo.findByUsername(jwtToken.getUsernameFromToken(token));
        return  author.getComments();
    }
}
