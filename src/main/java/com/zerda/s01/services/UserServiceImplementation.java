package com.zerda.s01.services;

import com.zerda.s01.config.JwtToken;
import com.zerda.s01.models.User;
import com.zerda.s01.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class UserServiceImplementation implements UserService {
    @Autowired
    private UserRepository userRepo;
    @Autowired
    JwtToken jwtToken;

    public void createUser(User newUser) {
        userRepo.save(newUser);
    };

    public ResponseEntity updateUser(Long id, User updatedUser, String token) {
        // Find an existing user object to be updated
        User existingUser = userRepo.findById(id).get();
        // Retrieve usernames
        String existingUsername = existingUser.getUsername();
        String authUsername = jwtToken.getUsernameFromToken(token);

        if (authUsername.equals(existingUsername)) {
            // Assign the new properties
            existingUser.setUsername(updatedUser.getUsername());
            String hashedPassword = new BCryptPasswordEncoder().encode(updatedUser.getPassword());
            existingUser.setPassword(hashedPassword);
            // Save the changes to the database
            userRepo.save(existingUser);
            return new ResponseEntity<>("An existing user account was updated.", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You're not authorized to update this user account.", HttpStatus.UNAUTHORIZED);
        }
    };

    public ResponseEntity deleteUser(Long id, String token) {
        // Retrieve the existing user object to be updated
        User existingUser = userRepo.findById(id).get();
        // Retrieve usernames
        String existingUsername = existingUser.getUsername();
        String authUsername = jwtToken.getUsernameFromToken(token);

        if (authUsername.equals(existingUsername)) {
            userRepo.deleteById(id);
            return new ResponseEntity<>("An existing user account was deleted.", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You're not authorized to delete this user account.", HttpStatus.UNAUTHORIZED);
        }
    };

    public Iterable<User> getUsers(String token) {
        // Retrieve an existing user object
        User existingUser = userRepo.findByUsername(jwtToken.getUsernameFromToken(token));

        if (existingUser != null) {
            return userRepo.findAll();
        }
        return null;
    };

    public Optional<User> findByUsername(String username) {
        return Optional.ofNullable(userRepo.findByUsername(username));
    }
}

